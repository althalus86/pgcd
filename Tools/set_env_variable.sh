# full connnection string to connect to postgres
CONNECTION_STRING="	postgres://fapmyije:P9ibIzeArlNxfpVru9-eX_lhIg2IsFkQ@manny.db.elephantsql.com:5432/fapmyije"
# this is a private token that has to be generated for the user of the repo
ACCESS_TOKEN="NxqCsxi7wsDLKpgXFy_k"
# project name is url-encoded :: / => %2F
GITLAB_URL="https://gitlab.com/api/v4/projects/althalus86%2Fpgcd/variables"
# environment name to create
ENV="STAGE"

# ----------------------------------------------------- #

# extract the protocol
proto="$(echo $CONNECTION_STRING | grep :// | sed -e's,^\(.*://\).*,\1,g')"
# remove the protocol
url="$(echo ${CONNECTION_STRING/$proto/})"
# extract the user (if any)
user="$(echo $url | grep @ | cut -d@ -f1)"
# extract the username (from password)
username="$(echo $user | grep : | cut -d: -f1)"
# extract the password 
password="$(echo $user | grep : | cut -d: -f2)"
# extract the host and port
hostport="$(echo ${url/$user@/} | cut -d/ -f1)"
# by request host without port    
host="$(echo $hostport | sed -e 's,:.*,,g')"
# by request - try to extract the port
port="$(echo $hostport | sed -e 's,^.*:,:,g' -e 's,.*:\([0-9]*\).*,\1,g' -e 's,[^0-9],,g')"
# extract the path (if any)
path="$(echo $url | grep / | cut -d/ -f2-)"

echo "  url: $url"
echo "  proto: $proto"
echo "  user: $user"
echo "  username: $username"
echo "  password: $password"
echo "  host: $host"
echo "  port: $port"
echo "  path: $path"

# curl --request POST --header "PRIVATE-TOKEN: $ACCESS_TOKEN" "$GITLAB_URL" --form "key=NEW_VARIABLE" --form "value=new value"
curl --request POST --header "PRIVATE-TOKEN: $ACCESS_TOKEN" "$GITLAB_URL" --form "key=DBCONN_$ENV" --form "value=$CONNECTION_STRING"
curl --request POST --header "PRIVATE-TOKEN: $ACCESS_TOKEN" "$GITLAB_URL" --form "key=DB_DBNAME_$ENV" --form "value=$path"
curl --request POST --header "PRIVATE-TOKEN: $ACCESS_TOKEN" "$GITLAB_URL" --form "key=DB_HOSTNAME_$ENV" --form "value=$host"
curl --request POST --header "PRIVATE-TOKEN: $ACCESS_TOKEN" "$GITLAB_URL" --form "key=DB_PASSWORD_$ENV" --form "value=$password"
curl --request POST --header "PRIVATE-TOKEN: $ACCESS_TOKEN" "$GITLAB_URL" --form "key=DB_USERNAME_$ENV" --form "value=$username"

# output variable set on server
curl --header "PRIVATE-TOKEN: $ACCESS_TOKEN" "$GITLAB_URL"
